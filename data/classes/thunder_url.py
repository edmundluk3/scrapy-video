# -*- coding: utf-8 -*-
import copy
from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from contextlib import contextmanager

from .base import Base

class ThunderUrl(Base):
    __tablename__ = 'thunder_url'

    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    season = Column(Integer)
    episode = Column(Integer)
    url_type = Column(String(50))
    url = Column(String(300))

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
