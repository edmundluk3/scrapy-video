FROM python:2.7.13
RUN mkdir /code 
ADD . /code/
WORKDIR /code
RUN pip install -r requirements.txt