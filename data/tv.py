# -*- coding: utf-8 -*-
import copy
from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from contextlib import contextmanager

## import session
from __init__ import engine, Session

from .classes.base import Base
from .classes.cast import Cast
from .classes.thunder_url import ThunderUrl
from .classes.tv import TV

Base.metadata.create_all(engine)

@contextmanager
def session_scope():
    """Provide a transactional scope around a series of operations."""
    session = Session()
    session.expire_on_commit = False
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()

def create(item):
    with session_scope() as session:
        tv = TV(**item)
        session.add(tv)

def query(offset=0, limit=10):
    if (offset < 0):
        offset = 0

    with session_scope() as session:
        return session.query(TV).order_by(TV.id)[offset:offset+limit]

def get(title):
    """
    for spider to use
    """
    with session_scope() as session:
        return session.query(TV).filter_by(title=title).one_or_none()

def detail(id):
    with session_scope() as session:
        return session.query(TV).filter(TV.id == id).one_or_none()

def count():
    with session_scope() as session:
        return session.query(TV).count()