# -*- coding: utf-8 -*-
from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from contextlib import contextmanager

from .base import Base
class Cast(Base):
    __tablename__ = 'cast'

    id = Column(Integer, primary_key=True)
    name = Column(String(50),nullable=False, unique=False)

    def __init__(self, name):
        self.name = name