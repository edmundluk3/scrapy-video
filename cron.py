import schedule
import time
import urllib
import urllib2
def job():
    print "I'm working..."

def schedule_job():
    response = urllib2.urlopen('http://localhost:6800/schedule.json', urllib.urlencode({
        'project': 'piaov',
        'spider': 'tv'
    }))
    print response.read()

schedule.every().hour.do(schedule_job)

while True:
    schedule.run_pending()
    time.sleep(1)
