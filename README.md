scrapy movies and tv shows

> scrapy https://doc.scrapy.org/en/1.3/

## Installation

### virtualenv required
```
pip install virtualenv
```

### activate virtualenv
```
virtualenv env
source env/bin/activate
```

### install packages
```
pip install -r requirements.txt
```

### deactivate virtualenv
```
deactivate
```

### Platform specific installation notes
#### windows
0. easy_install lxml
0. pip install Scrapy
0. install pywin32 as indicated by the docs

## Usage
0. start your PostgreSQL

1. start scrapy splash server

2. start scrapyd server
```
$ scapyd
```

3. deploy and eggify your spider to scrapyd
```
$ scrapyd-deploy
```

4. schedule your project to run
```
$ python cron.py
```

5. start web RESTful server
```
$ python restful/app.py
```

Or forget all above and run `docker-compose up`