# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.exceptions import DropItem
from data import tv
import jsonpickle, requests

class PiaovPipeline(object):
    def process_item(self, item, spider):
        return item

class DuplicatesPipeline(object):
    def __init__(self):
        self.urls_seen = set()
        
    def process_item(self, item, spider):
        # fixme
        # title is the unique key
        if item['title'] in self.urls_seen:
            raise DropItem("Duplicate item found: %s" % item['title'])
        else:
            self.urls_seen.add(item['title'])
            return item

class ExistsInDatabasePipeline(object):
    def process_item(self, item, spider):
        if tv.get(item['title']) is not None:
            raise DropItem('Item %s exits in Database already' % item['title'])
        else:
            return item

class DatabasePipeline(object):
    def process_item(self, item, spider):
        tv.create(item)
        return item

class ApiPipeline(object):
    def process_item(self, item, spider):
        req = {}
        for key in item.keys():
            req[key] = item.get(key, '')

        print '-' * 20
        print jsonpickle.encode(req)
        print '-' * 20

        r = requests.post('http://45.62.104.174/tv/',
                      data=jsonpickle.encode(req),
                      headers={'Content-Type': 'application/json;utf-8'})
        return r.json()