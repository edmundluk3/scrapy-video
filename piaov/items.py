# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class PiaovTVItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    title = scrapy.Field()
    year = scrapy.Field()
    location = scrapy.Field()
    update_status = scrapy.Field()
    cast = scrapy.Field()
    tv_type = scrapy.Field()
    audio = scrapy.Field()
    update_date = scrapy.Field(serializer=str)
    plot = scrapy.Field()
    thunders = scrapy.Field()