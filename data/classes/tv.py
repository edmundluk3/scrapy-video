# -*- coding: utf-8 -*-
from sqlalchemy import create_engine, Column, Integer, String, Text, ForeignKey, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from contextlib import contextmanager
import jsonpickle

from .base import Base

class TV(Base):
    __tablename__ = 'tv'

    id = Column(Integer, primary_key=True)
    title = Column(String(50))
    year = Column(String(50))
    location = Column(String(50))
    update_status = Column(String(50))
    tv_type = Column(String(50))
    audio = Column(String(50))
    update_date = Column(String(50))
    plot = Column(Text())

    cast = Column(Text())
    thunders = Column(Text())
    
    def __init__(self, title, update_status, tv_type,
         audio, update_date, plot, thunders, cast, location, year):
        self.title = title
        self.year = year
        self.update_date = update_date
        self.tv_type = tv_type
        self.audio = audio
        self.update_status = update_status
        self.plot = plot
        self.location = location

        self.cast = jsonpickle.encode(cast)
        self.thunders = jsonpickle.encode(thunders)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
    
    def __repr__(self):
        return "<Link (title='%s')" % (self.title )
