# -*- coding: utf-8 -*-
import scrapy
import re
from scrapy_splash import SplashRequest
from piaov.items import PiaovTVItem

class TvSpider(scrapy.Spider):
    name = "tv"

    # todo
    # add tv_type table in database
    start_urls = [
        'http://www.piaov.com/list/13.html',
        'http://www.piaov.com/list/1.html',
        'http://www.piaov.com/list/24.html',
        'http://www.piaov.com/list/25.html',
    ]   
    
    def parse(self, response):
        for m in response.css('#main ul.mlist li'):
            link = m.css('.info > h2 > a::attr(href)').extract_first()
            next_link = response.urljoin(link)

            yield SplashRequest(next_link, self.parse_tv)
        
        next_page = response.css('#pages .curr + a::attr(href)').extract_first()

        if next_page is not None:
            # builds a full absolute URL 
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)

    def parse_tv(self, response):

        #script = response.css('#main > div.endpage > div:nth-child(3) > div.ndownlist > ul > script::text').extract_first()
        #links = []
        #self.log(script)

        #if script is not None and re.search('GvodUrls', script):
        #    urls = re.match(r'[^"]+"([^"]+)"', script).group(1).split('###')

        #    for url in urls:
        #        if url:
        #            title = re.match(r'.+file\|([^\|]+)\|', url).group(1)
        #            thunder = url[url.find('thunder'):]
        #            links.append({
        #                'title': title,
        #                'thunder': thunder
        #            })

        links = []

        url_lists = response.css('#main > div.endpage div.ndownlist > ul > li')

        for i in url_lists:
            link = {
                'url': i.css('span a[href*=thunder]::attr(href)').extract_first(),
                'size': i.css('p::text').extract_first(),
                'name': i.css('p a::text').extract_first(),
            }

            if u'到剪贴板' not in link['name']:
                links.append(link)

        item = PiaovTVItem()

        item['title'] = response.css('#main .info > h1::text').extract_first(default='not found')
        item['update_status'] = re.sub(r'<[^<>]+?>', '',response.css('#main > div.view > div.info > ul > li:nth-child(1)').extract_first(default='not found'))
        item['cast'] = response.css('#main > div.view > div.info > ul > li:nth-child(2) a::text').extract()
        item['tv_type'] = response.css('#main > div.view > div.info > ul > li:nth-child(3) > a::text').extract_first(default='not found')
        item['location'] = response.css('#main > div.view > div.info > ul > li:nth-child(4) > a::text').extract_first(default='not found')

        # filter audio
        item['audio'] = re.sub(r'<[^<>]+?>', '', response.css('#main > div.view > div.info > ul > li:nth-child(3)').extract_first(default='not found'))
        audio_colon_index = item['audio'].rfind(u'：') + 1
        item['audio'] = item['audio'][audio_colon_index:]

        # filter year
        item['year'] = re.sub(r'<[^<>]+?>', '', response.css('#main > div.view > div.info > ul > li:nth-child(4)').extract_first(default='not found'))
        year_colon_index = item['year'].rfind(u'：') + 1
        item['year'] = item['year'][year_colon_index:]

        item['update_date'] = response.css('#main > div.view > div.info > ul > li:nth-child(5) > div::text').extract_first(default='not found')
        item['plot'] = response.css('#main > div.endpage > div.juqing > div.endtext > p::text').extract_first(default='not found')
        item['thunders'] = links

        yield item